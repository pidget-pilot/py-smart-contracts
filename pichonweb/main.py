import forms

from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)
"""
en este ejercicio se muestra  como renderizar un template de html,por default flask 
busca los html dentro de una carpeta que se llame templates, si es necesario cambiar ubicacion
hay que mandar el nombre de la carpeta desde que se instancia, es decir:
app = Flask(__name__, template_folder = "template_name_folder")
"""

@app.route('/', methods = ['GET','POST'])
def index(name='Pichones Corp'):
    comment_form = forms.CommentForm(request.form)
    if request.method == 'POST':
        print (comment_form.username.data)
        print (comment_form.email.data)
        print (comment_form.comment.data)
    return  render_template('index.html',title = name, form = comment_form)


if __name__ == "__main__":
    app.run(debug= True, port= 8000)